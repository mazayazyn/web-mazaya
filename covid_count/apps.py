from django.apps import AppConfig


class CovidCountConfig(AppConfig):
    name = 'covid_count'
