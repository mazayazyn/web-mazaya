from django.shortcuts import render
from django.http import JsonResponse
from datetime import datetime, timedelta, date, time
from django.forms.models import model_to_dict
from django.utils.timezone import make_aware
from .models import COVIDStat
import requests
import json
# Create your views here.

def index(request):
    context = json.loads(access_db_and_return_latest(request).content)
    return render(request, 'covid_count/index.html', context)

def table(request):
    context = {'database' : COVIDStat.objects.all()}
    return render(request, 'covid_count/table.html', context)

def access_db_and_return_latest(request):
    access_date = make_aware(datetime.now().replace(microsecond=0))
    access_for_url = access_date.replace(second=0, minute=0, hour=0)
    url = "https://api.covid19api.com/country/indonesia?from={}&to={}".format(
           access_for_url.isoformat() + 'Z',
           (access_for_url - timedelta(days=7)).isoformat() + 'Z')
    payload = {}
    headers= {}
    response = requests.request("GET", url, headers=headers, data = payload)
    json_list = json.loads(response.text.encode('utf8'))
    if len(json_list) > 1:
        dict_to_check = json_list[-1]
        temp = COVIDStat(date_accessed = access_date,
                         confirmed     = dict_to_check['Confirmed'],
                         deaths        = dict_to_check['Deaths'],
                         recovered     = dict_to_check['Recovered'],
                         active        = dict_to_check['Active'])
        access_db(temp)
    return JsonResponse(model_to_dict(COVIDStat.objects.latest('date_accessed')))
    
def access_db(temp):
    try:
        latest_change = tuple(temp.change(COVIDStat.objects.latest('date_accessed')).values())
        if latest_change != (0, 0, 0, 0):
            temp.save()
        else:
            del temp
    except COVIDStat.DoesNotExist:
        temp.save()

    

