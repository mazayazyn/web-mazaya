# from django.shortcuts import render
# from django.shortcuts import render, redirect
# from .models import KritikSaran
# from .forms import KritikSaranForm

# Create your views here.

# def AboutUs(request):
#     if request.method == "POST":
#         form = KritikSaranForm(request.POST)
#         if form.is_valid():
#             ks = KritikSaran()
#             ks.kritik_saran = form.cleaned_data['kritik_saran']
#             ks.save()
#             # return redirect('result/')
#         return render(request,'aboutUs/AboutUs.html',{'form' : form})
#     form = KritikSaran()
#     return render(request,'aboutUs/AboutUs.html',{'form' : form})

from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import KritikSaranForm
from .models import KritikSaran

def AboutUs(request):
    if request.method == 'POST':
        form = KritikSaranForm(request.POST)
        if form.is_valid():
            ks = KritikSaran()
            ks.kritik_saran = form.cleaned_data['kritik_saran']
            KritikSaran.objects.create(
                kritik_saran = request.POST.get('kritik_saran', ks.kritik_saran)
            )
            return HttpResponseRedirect('/aboutUs')
    form = KritikSaranForm()
    result = {'form':form.as_table, 'kss':KritikSaran.objects.filter().order_by('-date')[:3]}
    return render(request, 'aboutUs/aboutUs.html', result)

