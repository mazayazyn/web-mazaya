from django.db import models

# Create your models here.
class RumahSakit(models.Model):
	nama = models.CharField(max_length=50, blank=False)
	provinsi = models.CharField(max_length=30, blank=False)
	telepon = models.CharField(max_length=15, blank=False)
	alamat = models.CharField(max_length=200, blank=False)

	class Meta:
		ordering = ['provinsi', 'nama']

	def __str__(self):
		return self.nama