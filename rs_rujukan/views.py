from django.shortcuts import render, redirect
from django.db.models import Q
from .models import RumahSakit

# Create your views here.
def index(request):
	query = request.GET.get('f')
	if query is not None:
		data = RumahSakit.objects.filter(
			Q(nama__icontains=query)
		)
		response = {'data':data}
		return render(request, 'rs_rujukan/rs_rujukan.html', response)

	data = RumahSakit.objects.all()
	response = {'data':data}
	return render(request, 'rs_rujukan/rs_rujukan.html', response)