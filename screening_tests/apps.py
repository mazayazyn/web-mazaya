from django.apps import AppConfig


class ScreeningTestsConfig(AppConfig):
    name = 'screening_tests'