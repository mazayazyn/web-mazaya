from django.contrib import admin
from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


app_name = 'screening_tests'

urlpatterns = [
    path('', views.test, name='test'),
    path('result/', views.result, name='res'),
]

urlpatterns += staticfiles_urlpatterns()