from django.test import TestCase, Client
from .models import *
from .views import *


class UnitTestScreeningTests(TestCase):
    def test_screening_tests_is_exist(self):
        response = self.client.get('/screening_tests/')
        self.assertEqual(response.status_code, 200)

    def test_screening_res_redirect_if_database_is_empty(self):
        response = self.client.get('/screening_tests/result/')
        self.assertEqual(response.status_code, 302)

    def test_screening_test_using_correct_template(self):
        response = self.client.get('/screening_tests/')
        self.assertTemplateUsed(response, 'screening_tests/test.html', 'base.html')
    
    def test_screening_test_res_using_correct_template(self):
        Gejala.objects.create(
            demam = False,
            batuk = False,
            lelah = False,
            diare = False,
            sakit_kepala = False,
            sakit_tenggorokan = False,
            ruam = False,
            rasa_bau = False,
            sakit_dada = False,
            kesusahan_bernapas = False
        )
        response = self.client.get('/screening_tests/result/')
        self.assertTemplateUsed(response, 'screening_tests/result.html', 'base.html')
    
    def test_saving_form_and_display_result_zero(self):
        data = {
            'demam': False,
            'batuk': False,
            'lelah': False,
            'diare': False,
            'sakit_kepala': False,
            'sakit_tenggorokan': False,
            'ruam': False,
            'rasa_bau': False,
            'sakit_dada': False,
            'kesusahan_bernapas': False,
        }
        response = self.client.post('/screening_tests/', data)
        count_all_gejala = Gejala.objects.all().count()
        self.assertEqual(count_all_gejala, 1)

        self.assertEqual (response.status_code, 302)
        self.assertEqual (response['location'], '/screening_tests/result/')

        new_response = self.client.post('/screening_tests/result/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Anda tidak memiliki gejala COVID 19.', html_response)
    
    def test_saving_form_and_display_result_low(self):
        data = {
            'demam': True,
            'batuk': False,
            'lelah': False,
            'diare': False,
            'sakit_kepala': False,
            'sakit_tenggorokan': False,
            'ruam': False,
            'rasa_bau': False,
            'sakit_dada': False,
            'kesusahan_bernapas': False,
        }
        response = self.client.post('/screening_tests/', data)
        count_all_gejala = Gejala.objects.all().count()
        self.assertEqual(count_all_gejala, 1)

        self.assertEqual (response.status_code, 302)
        self.assertEqual (response['location'], '/screening_tests/result/')

        new_response = self.client.post('/screening_tests/result/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Hati-hati anda memiliki gejala COVID 19.', html_response)
    
    def test_saving_form_and_display_result_med(self):
        data = {
            'demam': True,
            'batuk': True,
            'lelah': True,
            'diare': True,
            'sakit_kepala': False,
            'sakit_tenggorokan': False,
            'ruam': False,
            'rasa_bau': False,
            'sakit_dada': False,
            'kesusahan_bernapas': False,
        }
        response = self.client.post('/screening_tests/', data)
        count_all_gejala = Gejala.objects.all().count()
        self.assertEqual(count_all_gejala, 1)

        self.assertEqual (response.status_code, 302)
        self.assertEqual (response['location'], '/screening_tests/result/')

        new_response = self.client.post('/screening_tests/result/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Anda rentan terjangkit COVID 19.', html_response)

    def test_saving_form_and_display_result_high(self):
        data = {
            'demam': True,
            'batuk': True,
            'lelah': True,
            'diare': True,
            'sakit_kepala': False,
            'sakit_tenggorokan': False,
            'ruam': False,
            'rasa_bau': False,
            'sakit_dada': True,
            'kesusahan_bernapas': False,
        }
        response = self.client.post('/screening_tests/', data)
        count_all_gejala = Gejala.objects.all().count()
        self.assertEqual(count_all_gejala, 1)

        self.assertEqual (response.status_code, 302)
        self.assertEqual (response['location'], '/screening_tests/result/')

        new_response = self.client.post('/screening_tests/result/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Anda sangat rentan terjangkit COVID 19.', html_response)
        