from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.mainpage, name='mainpage'),
    path('story1/', views.home2, name='home2'),
    path('story5/', views.home, name='home'),
    path('story5/cv/', views.cv, name='cv'),
]