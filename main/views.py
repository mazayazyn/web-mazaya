from django.shortcuts import render
from django.http import HttpResponse


def mainpage(request):
    return render(request, "mainpage.html")

def home(request):
    name = "azza"
    return render(request, "home.html", {'name':name})

def home2(request):
    return render(request, "home2.html")

def cv(request):
    return render(request, "cv.html")