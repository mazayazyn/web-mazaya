from django.urls import path
from django.contrib import admin
from . import views

app_name = 'edukasi'

urlpatterns = [
    path('', views.edukasi, name='edukasi'),
]