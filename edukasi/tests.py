from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from .models import Opinion
from .views import edukasi
from .forms import OpinionForm
from .apps import EdukasiConfig

class ModelsTest(TestCase):
    def setUp(self):
        self.opini = Opinion.objects.create(opini="Cara penyebaran COVID-19 sangat berbahaya")

    def test_opini_created(self):
        count_opini = Opinion.objects.all().count()
        self.assertEquals(count_opini, 1)

    def test_str_opini(self):
        self.assertEquals(str(self.opini), "Cara penyebaran COVID-19 sangat berbahaya")

class URLsTest(TestCase):
    def test_edukasi_url_exists(self):
        response = Client().get('/edukasi/')
        self.assertEquals(response.status_code, 200)

class HTMLTest(TestCase):
    def test_edukasi_html_template_exists(self):
        response = Client().get('/edukasi/')
        self.assertTemplateUsed(response, 'edukasi/edukasi.html')

class ViewsTest(TestCase):
    def test_edukasi_view_func(self):
        found = resolve('/edukasi/')
        self.assertEquals(found.func, edukasi)

    def test_get_edukasi_func(self):
        response = Client().get('/edukasi/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'edukasi/edukasi.html')

    def test_opini_valid_then_redirect(self):
        response = Client().post('/edukasi/', data={'opini_form': 'covid menakutkan'}, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'edukasi/edukasi.html')

    def test_opini_invalid_then_redirect(self):
        response = Client().post('/edukasi/', data={'opini_form': ' '}, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'edukasi/edukasi.html')

class FormsTest(TestCase):
    def setUp(self):
        self.opini = Opinion.objects.create(opini="Cara penyebaran COVID-19 sangat berbahaya")

    def test_form_valid(self):
        form_opini = OpinionForm(data={'opini_form': self.opini})
        self.assertTrue(form_opini.is_valid())

    def test_form_invalid(self):
        form_opini = OpinionForm(data={})
        self.assertFalse(form_opini.is_valid())

class AppTest(TestCase):
    def test_edukasi_app(self):
        self.assertEquals(EdukasiConfig.name, 'edukasi')
        self.assertEquals(apps.get_app_config('edukasi').name, 'edukasi')